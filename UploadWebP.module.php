<?php namespace ProcessWire;

class UploadWebP extends WireData implements Module, ConfigurableModule {

    protected $failMessage;
    public $outputFormat;
    public $jpegQuality;
    public $pngCompressionLevel;

    /**
     * Construct
     */
    public function __construct() {
        parent::__construct();
        $this->failMessage = _('Unable to use the UploadWebp module, Imagick or GD required.');
        $this->outputFormat = 'png';
        $this->jpegQuality = 90;
        $this->pngCompressionLevel = 9;
    }

    /**
     * Ready
     */
    public function ready() {
        $this->addHookBefore('InputfieldFile::processInputAddFile', $this, 'beforeImageAdded');
    }

    /**
     * Before InputfieldFile::processInputAddFile
     *
     * @param HookEvent $event
     */
    protected function beforeImageAdded(HookEvent $event) {

        $inputfield = $event->object;
        if(!$inputfield instanceof InputfieldImage) return;        

        $basename = $event->arguments(0);
        $ext = strtolower(pathinfo($basename, PATHINFO_EXTENSION));
        if('webp' !== $ext) return;

        $pageimages = $inputfield->value;

        $filename = $pageimages->path() . $basename;
        $path_parts = pathinfo($filename);
        $dirname = $path_parts['dirname'] . '/';

        $ext = $this->outputFormat;

        // Basename for converted image
        $basename = $path_parts['filename'] . '.' . $ext;

        // Adjust basename if it will clash with an existing file
        $i = 1;
        while(is_file($dirname . $basename)) {            
            $basename = $path_parts['filename'] . '-' . $i++ . '.' . $ext;
        }

        $new_filename = $dirname . $basename;

        switch($this->outputFormat) {
            case 'JPG':
                $event->arguments(0, $this->convertToJPEG($filename, $new_filename));
                break;
            default: // PNG
                $event->arguments(0, $this->convertToPNG($filename, $new_filename));
        }

        // Delete original
        unlink($filename);		
    }

    /**
     * Convert the supplied image to JPEG format
     *
     * @param string $filename
     * @return string
     */
    protected function convertToJPEG($filename, $new_filename) {

        if(extension_loaded('imagick')) {
            $image = new \Imagick($filename);
            // Not sure if the following are needed but just in case
            $image->setImageBackgroundColor('white');
            $image->mergeImageLayers(\Imagick::LAYERMETHOD_FLATTEN); // Flatten layers if present
            $image->setImageAlphaChannel(\Imagick::ALPHACHANNEL_REMOVE); // Avoid transparent areas becoming black
            $image->setImageCompression(\Imagick::COMPRESSION_JPEG);
            $image->setImageCompressionQuality($this->jpegQuality);
            $image->setImageFormat('jpg');
            $image->writeImage($new_filename);
        } else {
            if(!function_exists('imagecreatefromwebp')) {
                $this->wire()->session->error($this->failMessage);
                return $filename;
            }
            $image = imagecreatefromwebp($filename);
            imagejpeg($image, $new_filename, $this->jpegQuality);
            imagedestroy($image);
        }		

        return $new_filename;
    }
    
    /**
     * Convert the supplied image to PNG format
     * 
     * @param string $filename
     * @return string
     */ 
    protected function convertToPNG($filename, $new_filename) {

        if(extension_loaded('imagick')) {
            $image = new \Imagick($filename);
            $image->setImageFormat('png');
            $image->setOption('png:compression-level', $this->pngCompressionLevel);
            $image->writeImage($new_filename);
        } else {
            if(!function_exists('imagecreatefromwebp')) {
                $this->wire()->session->error($this->failMessage);
                return $filename;
            }
            $image = imagecreatefromwebp($filename);
            imagepng($image, $new_filename, $this->pngCompressionLevel);
            imagedestroy($image);
        }

        return $new_filename;
    }

    /**
     * Config inputfields
     *
     * @param InputfieldWrapper $inputfields
     */
    public function getModuleConfigInputfields($inputfields) {

        /* @var InputfieldSelect $f1 */
        $f1 = $this->wire()->modules->get('InputfieldSelect');
        $f1->name = 'outputFormat';
        $f1->label = $this->_('Output Format');
        $f1->inputType = 'select';
        $f1->addOptions(['png' => 'PNG', 'jpg' => 'JPEG']);
        $f1->value = $this->outputFormat;
        $f1->notes = 'Choose PNG if your images will contain transparent areas';
        $inputfields->add($f1);
        //$f1->setAndSave('columnWidth', 50);

        /* @var InputfieldInteger $f2 */
        $f2 = $this->wire()->modules->get('InputfieldInteger');
        $f2->name = 'jpegQuality';
        $f2->label = $this->_('Quality for JPG conversion (0..99)');
        $f2->inputType = 'number';
        $f2->setAttribute('min', '0');
        $f2->setAttribute('max', '99');
        $f2->value = $this->jpegQuality;
        $f2->notes = 'Larger number = larger file and less quality reduction.';
        $f2->showIf = "outputFormat=jpg";
        $inputfields->add($f2);

        /* @var InputfieldInteger $f3 */
        $f3 = $this->wire()->modules->get('InputfieldInteger');
        $f3->name = 'pngCompressionLevel';
        $f3->label = $this->_('PNG compression level (1..9)');
        $f3->inputType = 'number';
        $f3->setAttribute('min', '1');
        $f3->setAttribute('max', '9');
        $f3->notes = 'Larger number will result in smaller files.  NOTE: quality will **not** be reduced.';        
        $f3->value = $this->pngCompressionLevel;
        $f3->showIf = "outputFormat=png";
        $inputfields->add($f3);
    }

    /**
     * Install
     */
    public function ___install() {
        if(!extension_loaded('imagick') && !function_exists('imagecreatefromwebp')) {
            $this->wire()->error($this->failMessage);
        }        
    }
    /**
     * getModuleInfo()
     * @return array
     */

    public static function getModuleInfo() {
        return [
            'title' => 'Upload WebP',
            'version' => '0.1.4',
            'author' => 'martin@applab.co.uk',
            'summary' => 'Converts WebP images to PNG or JPG on upload. (based on https://processwire.com/modules/webp-to-jpg/)',
            'icon' => 'upload',
            'href' => 'https://gitlab.com/applab/pw-uploadwebp/',
            'singular' => true,
            'autoload' => true,
            'requires' => [           
                'PHP>=7.1',
                'ProcessWire>=3.0.0'
            ],
        ];
    }

}