# PW UploadWebP

Processwire module to convert WebP images to PNG or JPEG on upload.

(Processwire's InputfieldImage does not support WebP as an input format out of the box)

This module is based on https://github.com/Toutouwai/WebpToJpg

## Installation

At this time the module is not available in the Processwire Modules Directory and must be installed manually.

1) Copy the file `UploadWebP.module.php` to your `/sites/modules/` folder.
NOTE: If you prefer to keep each module in it's own folder then create a folder called `UploadWebP` under `/sites/templates/` and place the module file there.
2) In Processwire admin click `Modules -> Refresh`, then `Modules -> Site`
3) Locate the `Upload WebP` module and click the `Install` button.
4) You are now ready to configure the module.

## Config

First, select your preferred Output Format`, either PNG or JPEG. If you are using images with transparent regions you _must_ choose PNG.

If PNG is selected: choose a compression level between 1 and 9 (default: 9).
    
> PNG is a lossless format, choosing a higher number will result in a smaller file but will **not** reduce image quality.

If JPEG is chosen: choose a compression level between 0 and 99 (default: 90). 

> NOTE: Lower numbers will result in a smaller file size but lower image quality.

## Important

Any image field for which you need to enable WebP uploads must have `webp` added to `Allowed file extensions` on the `Details` tab of the fields settings page.

